module Rosby.Config where

import Conferer
import Data.Text(Text())
import GHC.Generics(Generic)

data RosbyConfig
    = RosbyConfig
    { rosbyConfigHost :: Text
    , rosbyConfigPort :: Int
    }
    deriving (Eq, Show, Generic)
instance FromConfig RosbyConfig

instance DefaultConfig RosbyConfig where
  configDef = RosbyConfig
    { rosbyConfigHost = "localhost"
    , rosbyConfigPort = 1993
    }
module Rosby.Server where

import Conferer
import Control.Concurrent
import Control.Exception
import Control.Monad
import Control.Monad.Logger
import Control.Monad.Reader
import qualified Data.Text as T
import Network.Socket
import qualified Network.Socket.ByteString as S
import System.IO

import Rosby.Config

data Context
    = Context
    { _contextConfig :: RosbyConfig
    }
    deriving (Eq, Show)

newtype Server a = Server { runServer :: ReaderT Context (LoggingT IO) a }
    deriving
    ( Applicative
    , Functor
    , Monad
    , MonadLogger
    , MonadReader Context
    , MonadIO
    )

server :: Socket -> Server ()
server sock = do
    (Context (RosbyConfig host port)) <- ask
    $(logDebug) ("Rosby, reporting for duty " <> host <> ":" <> (T.pack . show $ port))
    liftIO $ hFlush stdout
    -- TODO (james): replace this with an actual loop
    liftIO $ loop
    where
        loop = do
            putStr "> "
            hFlush stdout
            cmd <- getLine
            unless (cmd == "quit") $ loop

resolve :: HostName -> ServiceName -> IO AddrInfo
resolve host port = do
    let hints
            = defaultHints 
            { addrFlags = [AI_PASSIVE]
            , addrSocketType = Stream
            }
    head <$> getAddrInfo (Just hints) (Just host) (Just port)

open :: AddrInfo -> IO Socket
open addr = do
    sock <- socket (addrFamily addr) (addrSocketType addr) (addrProtocol addr)
    setSocketOption sock ReuseAddr 1
    withFdSocket sock $ setCloseOnExecIfNeeded
    bind sock $ addrAddress addr
    listen sock 1024
    return sock

start :: IO ()
start = do
    configSource <- mkConfig "rosby"
    rosbyConfig <- fetch configSource :: IO RosbyConfig
    let defaultContext = Context rosbyConfig
    addr <- resolve (T.unpack . rosbyConfigHost $ rosbyConfig) (show . rosbyConfigPort $ rosbyConfig)
    bracket (open addr) close $ \sock -> do
        runStdoutLoggingT $ runReaderT (runServer $ server sock) defaultContext
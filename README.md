# rosby

Rosby is a key-value store in Haskell. We are following James King (youtube.com/@jkennethking)'s videos, starting with 'Rosby Day 1 - Building a Key-Value Database from Scratch in Haskell' youtube.com/watch?v=qnPn-c_JF5g.

So far I have simply run 'stack new rosby', edited package.yaml to change the author name and email, and edited README.md.